printf '\e[8;27;74t'

java -jar ascii-game.jar | sed -e "
s/M\([^Po]\|$\)/\x1b[1;38;5;45m&\x1b[0m/g; 
s/D/\x1b[1;38;5;189m&\x1b[0m/g; 
s/R/\x1b[1;38;5;124m&\x1b[0m/g; 
s/G/\x1b[1;38;5;226m&\x1b[0m/g; 
s/V/\x1b[1;38;5;94m&\x1b[0m/g; 
s/A/\x1b[1;38;5;82m&\x1b[0m/g; 
s/X/\x1b[1;38;5;256m&\x1b[0m/g; 
s/HP:/\x1b[38;5;82m&\x1b[0m/g; 
s/MP:/\x1b[38;5;45m&\x1b[0m/g; 
s/-/\x1b[48;5;94;38;5;94m&\x1b[0m/g" 
