package edu.udel.cs.stages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import edu.udel.cs.entities.Apple;
import edu.udel.cs.entities.Entity;
import edu.udel.cs.entities.Hero;
import edu.udel.cs.entities.ManaPotion;
import edu.udel.cs.entities.Rock;
import edu.udel.cs.entities.Treasure;
import edu.udel.cs.utils.StringUtils;

public class InGameStage extends AbstractStage {
	private double dropRateRocksMin;
	private double dropRateRocksMax;
	private double dropRateApple;
	private double dropRateManaPotion;
	private double dropRateTreasures;
	private int speed; // Lower is faster;
	private int updatedCounter = 0;
	private int timeRemaining;
	private char[][] frameMatrix;
	private String dashLine;
	private Collection<Entity> fallingObjects = new LinkedList<>();
	private Collection<Entity> objectsOnBottom = new ArrayList<>(width);

	public InGameStage(int width, int height, Hero hero, double dropRateRocksMin, double dropRateRokcsMax, double dropRateApple, double dropRateManaPotion, double dropRateTreasures,  int speed, int time, int stageNumber) {
		super(stageNumber, width, height, hero);
		this.width = width;
		this.height = height;
		this.dropRateRocksMin = dropRateRocksMin;
		this.dropRateRocksMax = dropRateRokcsMax;
		this.speed = speed;
		this.hero = hero;
		this.hero.setPosition(width / 2, height - 1);
		this.dashLine = StringUtils.repeat("-", width);
		this.frameMatrix = new char[height][width];
		this.timeRemaining = time;
		this.stageNumber = stageNumber;
		this.dropRateApple = dropRateApple;
		this.dropRateManaPotion = dropRateManaPotion;
		this.dropRateTreasures = dropRateTreasures;
	}

	private void startTimer() {
		final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                timeRemaining--;
                if (timeRemaining < 0){
                    timer.cancel();
                }
            }
        }, 0, 1000);
	}

	/**
	 * Updates the game stage.
	 */
	public void update() {
		clearFrame();

		if (shouldUpdateFallingObjects()) {
			updateFallingObjects();
		}
		
		addEntityToFrame(hero);

		for (Entity object : fallingObjects) {
			addEntityToFrame(object);
		}
	}

	private void updateFallingObjects() {
		// Update objects's position.
		for (Entity object : fallingObjects) {
			if (object.getY() < height - 1) {
				object.fall();
			} else {
				objectsOnBottom.add(object);
			}
		}

		// Remove objects that reached the bottom.
		for (Entity object : objectsOnBottom) {
			fallingObjects.remove(object);
		}

		// Check for collisions.
		Entity collisionEntity = hero.collidesWithAny(objectsOnBottom);
		if (collisionEntity != null) {
			onHeroCollision(collisionEntity);
		}

		objectsOnBottom.clear();

		// Add new objects (rocks, apples, etc)
		int numberNewRocks = (int) ((Math.random() * width * dropRateRocksMax) + width * dropRateRocksMin);
		for (int i = 0; i < numberNewRocks; i++) {
			fallingObjects.add(new Rock((int) (Math.random() * width), 0));
		}

		int numberNewApples = (int) ((Math.random() * width * dropRateApple));
		for (int i = 0; i < numberNewApples; i++) {
			fallingObjects.add(new Apple((int) (Math.random() * width), 0));
		}
		
		int numberNewManaPotion = (int) ((Math.random() * width * dropRateManaPotion));
		for (int i = 0; i < numberNewManaPotion; i++) {
			fallingObjects.add(new ManaPotion((int) (Math.random() * width), 0));
		}
		
		int numberNewTreasures = (int) ((Math.random() * width * dropRateTreasures));
		for (int i = 0; i < numberNewTreasures; i++) {
			fallingObjects.add(new Treasure((int) (Math.random() * width), 0));
		}
	}

	private boolean shouldUpdateFallingObjects() {
		if (updatedCounter == speed) {
			updatedCounter = 0;
			return true;
		} else {
			updatedCounter++;
			return false;
		}
	}

	private void clearFrame() {
		for (char[] arr : frameMatrix) {
			Arrays.fill(arr, ' ');
		}
	}

	private void addEntityToFrame(Entity entity) {
		if(frameMatrix[entity.getY()][entity.getX()] == ' '){
			frameMatrix[entity.getY()][entity.getX()] = entity.getSymbol();
		}
	}

	@Override
	public String getFrame() {
		StringBuilder str = new StringBuilder();

		for (char[] arr : frameMatrix) {
			for (char symbol : arr) {
				str.append(symbol);
			}
			str.append(System.lineSeparator());
		}

		str.append(dashLine);

		return str.toString();
	}

	public IStage nextStage() {
		if(this.timeRemaining < 0){
			return nextLinkedStage;
		} else if (this.hero.getHP() <= 0) {
			return new GameOverStage(width, height, hero);
		} else {
			return this;
		}
	}

	private void onHeroCollision(Entity collisionEntity) {
		collisionEntity.onCollisionWith(hero);
	}

	@Override
	public void handleInput(char input) {
		switch (input) {
		case 'j':
			hero.moveLeft();
			break;
		case 'k':
			hero.moveRight();
			break;
		case 'l':
			if(hero.hasMp()){
				hero.decreaseMP();
				freezeObjects();
			}
		}
	}

	private void freezeObjects() {
		this.updatedCounter -= 50;
	}
	
	public void start(){
		startTimer();
	}

	@Override
	public int getTimeRemaining() {
		return timeRemaining;
	}
}
