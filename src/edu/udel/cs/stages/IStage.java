package edu.udel.cs.stages;

import edu.udel.cs.utils.IPrintable;

/**
 * 
 * @author Default behavior for Stages.
 *
 */
public interface IStage extends IPrintable {
	public void start();
	
	public IStage nextStage();

	public void setNextLinkedStage(IStage nextLinkedStage);

	public void update();

	public void handleInput(char input);

	public int getStageNumber();

	public int getTimeRemaining();
}