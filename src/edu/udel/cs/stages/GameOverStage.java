package edu.udel.cs.stages;

import edu.udel.cs.entities.Hero;
import edu.udel.cs.utils.StringUtils;

public class GameOverStage extends AbstractStage{
	private boolean restart;
	private String columns;
	private String linesBreaks;

	public GameOverStage(int width, int height, Hero hero) {
		super(width, height, hero);
		this.columns = StringUtils.repeat(" ", (width/2)-9);
		this.linesBreaks = StringUtils.repeat(System.lineSeparator(), (height/2)-2);
	}

	@Override
	public String getFrame() {
		return linesBreaks + 
			   columns + "\tGAME OVER\n" 
			 + columns + "Player's Score: " + hero.getScore()   + "\n"
			 + columns + " j = Play Again.\n"
			 + columns + " q = Quit\n" 
			 + linesBreaks;
	}

	@Override
	public AbstractStage nextStage() {
		if(restart){
			return new InitialStage(width, height, hero);
		}
		return this;
	}

	@Override
	public void update() {}

	@Override
	public void handleInput(char input) {
		if(input == 'j'){
			restart = true;
		} 
	}
}
