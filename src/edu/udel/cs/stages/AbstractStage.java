package edu.udel.cs.stages;

import edu.udel.cs.entities.Hero;

public abstract class AbstractStage implements IStage {
	protected IStage nextLinkedStage;
	protected int stageNumber;
	protected int height;
	protected int width;
	protected Hero hero;

	public AbstractStage(int stageNumber, int width, int height, Hero hero) {
		super();
		this.stageNumber = stageNumber;
		this.height = height;
		this.width = width;
		this.hero = hero;
	}

	public AbstractStage(int width, int height, Hero hero) {
		this(0, width, height, hero);
	}

	public int getTimeRemaining() {
		return 0;
	};

	public final int getStageNumber() {
		return stageNumber;
	}

	public final void setNextLinkedStage(IStage nextLinkedStage) {
		this.nextLinkedStage = nextLinkedStage;
	}
	
	public void start(){}
	
	public void update(){};

	public abstract IStage nextStage();

	public abstract void handleInput(char input);
}
