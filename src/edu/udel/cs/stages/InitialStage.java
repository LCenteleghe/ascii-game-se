package edu.udel.cs.stages;

import edu.udel.cs.entities.Hero;

public class InitialStage extends AbstractStage {
	private boolean start;
	private boolean information;
	private String Information;

	public InitialStage(int width, int height, Hero hero) {
		super(width, height, hero);
		setNextLinkedStage(GameStagesSet.getLinkedInGameStages(width, height, hero));
		hero.initStatus();

		this.Information = "\t\t IMPORTANT INSTRUCTIONS ABOUT THIS GAME!!!"
				+ "\n\nIn this game, you need to avoid rocks and catch Gold, Ruby, and Diamond."
				+ "\n\nHero will lose HP when he is hit by a rock," + "\nand recover HP when eat an apple."
				+ "\n\n'X' is the hero you need to control, 'A' is an apple, 'V' is a rock,"
				+ "\n'G' represents Gold (10 points), 'R' represents Ruby (50 points), and"
				+ "\n 'D' represents a Diamond (150 points).\n\nEnjoy your game!!\n\n\n\n\n\n\n\n\n\n\n\n\n"
				+ "\ntype 'k' to the firstpage            type 'j' to start the game";

	}

	@Override
	public String getFrame() {
		if (information) {
			return this.Information;
		} else {
			return "\\    /" + "\n \\  /" + "\n  \\/      \\    /" + "\n           \\  /"
					+ "\n            \\/      \\    /" + "\n                     \\  /"
					+ "\n                      \\/      \\    /" + "\n                               \\  /"
					+ "\n                                \\/      \\    /"
					+ "\n                                         \\  /"
					+ "\n                                          \\/"
					+ "\n      Press 'j' to Start the Game                "
					+ "\n                                              Rock is coming, HELP!!!!!"
					+ "\n      Press 'k' to see INSTRUCTIONS         __   /"
					+ "\n                                           /  \\"
					+ "\n                                           \\__/"
					+ "\n                                          __||__/"
					+ "\n                                         /  || "
					+ "\n                                         __/ \\"
					+ "\n                                             /" + "\n";
		}
	}

	@Override
	public IStage nextStage() {
		if (start) {
			return nextLinkedStage;
		} else {
			return this;
		}
	}

	@Override
	public void handleInput(char input) {
		if (input == 'j') {
			start = true;
		} else if (input == 'k') {
			this.information = !this.information;
		}
	}
}
