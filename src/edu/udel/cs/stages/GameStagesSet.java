package edu.udel.cs.stages;

import java.util.ArrayList;
import java.util.List;

import edu.udel.cs.entities.Hero;

public class GameStagesSet {
	private static AbstractStage linkStages(List<AbstractStage> stages) {
		for (int i = 0; i < stages.size() - 1; i++) {
			stages.get(i).setNextLinkedStage(stages.get(i + 1));
		}
		return stages.get(0);
	}
	
	public static AbstractStage getLinkedInGameStages(int width, int height, Hero hero){
		return getInGameStagesChain(width, height, hero, true);
	}

	public static AbstractStage getInGameStagesChain(int width, int height, Hero hero, boolean appendGameOver) {
		List<AbstractStage> stages = new ArrayList<>();

		stages.add(new InGameStage(width, height, hero, 
				0.0100, /* dropRateRockMin */
				0.1000, /* dropRateRockMax */
				0.0150, /* dropRateApple */
				0.0145, /* dropRateManaPotion */
				0.0250, /* dropRateTreasures */
				5, /* speed */
				30, /* time */
				stages.size() + 1));/* stageNumber */

		stages.add(new InGameStage(width, height, hero, 
				0.0110, /* dropRateRockMin */
				0.1000, /* dropRateRockMax */
				0.0145, /* dropRateApple */
				0.0142, /* dropRateManaPotion */
				0.0250, /* dropRateTreasures */
				3, /* speed */
				30, /* time */
				stages.size() + 1));/* stageNumber */
		
		stages.add(new InGameStage(width, height, hero, 0.01, /* dropRateRockMin */
				0.1015, /* dropRateRockMax */
				0.0140, /* dropRateApple */
				0.0145, /* dropRateManaPotion */
				0.0250, /* dropRateTreasures */
				1, /* speed */
				30, /* time */
				stages.size() + 1));/* stageNumber */

		if(appendGameOver){
			stages.add(new GameOverStage(width, height, hero));
		}

		return linkStages(stages);
	}
}
