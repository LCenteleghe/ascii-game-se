package edu.udel.cs.entities;
public class Rock extends Entity{
	public Rock(int x, int y, char symbol) {
		super(x, y, symbol);
	}
	
	public Rock(int x, int y) {
		super(x, y, 'V');
	}
	
	public void onCollisionWith(Hero hero){
		hero.loseHP();
	}
}
