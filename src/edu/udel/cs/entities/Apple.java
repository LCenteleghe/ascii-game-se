package edu.udel.cs.entities;

public class Apple extends Entity {
	public Apple(int x, int y, char symbol) {
		super(x, y, symbol);
	}
	
	public Apple(int x, int y) {
		super(x, y, 'A');
	}
	
	public void onCollisionWith(Hero hero){
		hero.addHP();
	}
}