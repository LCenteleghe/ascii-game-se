package edu.udel.cs.entities;

public class ManaPotion extends Entity{

	public ManaPotion(int x, int y, char symbol) {
		super(x, y, symbol);
	}
	
	public ManaPotion(int x, int y) {
		super(x, y, 'M');
	}
	
	public void onCollisionWith(Hero hero){
		hero.addMP();
	}
}
