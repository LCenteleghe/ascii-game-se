package edu.udel.cs.entities;
import java.util.Collection;

public class Entity {
	protected int x;
	protected int y;
	protected char symbol;

	public Entity(int x, int y, char symbol) {
		super();
		this.x = x;
		this.y = y;
		this.symbol = symbol;
	}

	public void fall(int numberOfBlocks) {
		y += numberOfBlocks;
	}

	public void fall() {
		y++;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public char getSymbol() {
		return symbol;
	}

	public void moveLeft() {
		this.x--;
	}

	public void moveRight() {
		this.x++;
	}

	public boolean collides(Entity entity) {
		return this.getX() == entity.getX() && this.getY() == entity.getY();
	}
	
	public void setPosition(int x, int y){
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns the entity the entity collided with. Or null if no collision happened.
	 */
	public Entity collidesWithAny(Collection<Entity> entities) {
		for (Entity entity : entities) {
			if (this.collides(entity)) {
				return entity;
			}
		}
		return null;
	}

	public void onCollisionWith(Entity entity) {
	}
	
	public void onCollisionWith(Hero entity) {
	}
}
