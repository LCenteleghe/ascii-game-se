package edu.udel.cs.entities;

public class Treasure extends Entity {
	private int value;

	public Treasure(int x, int y) {
		super(x, y, 'G');

		double rdm = Math.random();
		if (rdm < 0.7) {
			this.symbol = 'G'; // Gold
			this.value = 10;
		} else if (rdm < 0.958) {
			this.symbol = 'R'; // Ruby
			this.value = 50;
		} else {
			this.symbol = 'D'; // Diamond
			this.value = 150;
		}
	}
	
	public void onCollisionWith(Hero hero){
		hero.increaseScore(this.getValue());
	}

	public int getValue() {
		return this.value;
	}
}
