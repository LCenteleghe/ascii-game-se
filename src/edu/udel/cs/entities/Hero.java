package edu.udel.cs.entities;

public class Hero extends Entity {
	private static final int DEFAULT_MP = 3;
	private static final int DEFAULT_HP = 5;
	
	private int rightBound;
	private int score;
	private int hp;
	private int mp;

	public Hero(char symbol, int hp, int rightBound) {
		super(0, 0, symbol);
		this.hp = hp;
		this.rightBound = rightBound;
	}

	public Hero(char symbol, int hp, int mp, int rightBound) {
		super(0, 0, symbol);
		this.hp = hp;
		this.mp = mp;
		this.rightBound = rightBound;
	}

	public Hero(int rightBound) {
		super(0, 0, 'X');
		initStatus();
		this.rightBound = rightBound;
	}
	
	public void initStatus(){
		this.hp = DEFAULT_HP;
		this.mp = DEFAULT_MP;
		this.score = 0;
	}

	public void loseHP() {
		this.hp--;
	}

	public int getHP() {
		return this.hp;
	}

	public void addHP() {
		this.hp++;
	}

	public int getMP() {
		return this.mp;
	}

	public void addMP() {
		this.mp++;
	}

	public void increaseScore(int amount) {
		this.score += amount;
	}

	public void moveLeft() {
		if (super.x > 0) {
			super.moveLeft();
		}
	}

	public void moveRight() {
		if (super.x < rightBound) {
			super.moveRight();
		}
	}

	public boolean hasMp() {
		return mp > 0;
	}

	public void decreaseMP() {
		mp--;
	}

	public int getScore() {
		return this.score;
	}
}
