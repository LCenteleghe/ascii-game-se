package edu.udel.cs.game;

import java.util.LinkedList;
import java.util.List;

import edu.udel.cs.stages.IStage;
import edu.udel.cs.utils.IPrintable;

public class Screen {
	private List<IPrintable> printableObjects = new LinkedList<>();
	private IStage stage;
	private String lineDivider;

	public Screen() {
		super();
		this.lineDivider = new String();
	}

	public Screen(String lineDivider, IStage stage) {
		super();
		this.lineDivider = lineDivider;
		this.stage = stage;
	}

	public void setStage(IStage stage){
		this.stage = stage;
	}

	public void print() {
		for (IPrintable iPrintable : printableObjects) {
			System.out.println(lineDivider);
			System.out.println(iPrintable.getFrame());
		}
		
		System.out.println(lineDivider);
		System.out.println(stage.getFrame());
	}

	public void addPrintableObject(IPrintable iPrintable) {
		printableObjects.add(iPrintable);
	}
}
