package edu.udel.cs.game;

import java.io.IOException;
import java.io.PrintWriter;

import edu.udel.cs.entities.Hero;
import edu.udel.cs.stages.IStage;
import edu.udel.cs.stages.InitialStage;
import edu.udel.cs.utils.StringUtils;
import jline.ConsoleReader;
/**
 * Group Members:
 * 
 * Luis Gustavo Simioni Centeleghe
 * Jiyang Li
 * Xiomeng Chen
 * Talha Ehtasham
 */
public class Game {
	final static int WIDTH = 74;
	final static int HEIGHT = 20;

	private StatusBar statusBar;
	private Hero hero;
	private Screen screen;
	private IStage currentStage;

	public Game() {
		hero = new Hero(WIDTH - 1);
		currentStage = new InitialStage(WIDTH, HEIGHT, hero);
		statusBar = new StatusBar(WIDTH, hero, currentStage);

		screen = new Screen(StringUtils.repeat("_", WIDTH), currentStage);
		screen.addPrintableObject(statusBar);
	}

	public void start() {
		startGameUpdateLoop();
		handleInput();
	}
	
	private void handleInput() {
		try {
			char[] allowed = { 'i', 'j', 'k', 'l', 'q' };
			ConsoleReader reader = new ConsoleReader(System.in, new PrintWriter(System.out));
			int input = 0;
			while (true) {
				input = reader.readCharacter(allowed);
				switch (input) {
				case 'q':
					System.exit(0);
				default:
					currentStage.handleInput((char) input);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void startGameUpdateLoop() {
		new Thread() {
			public void run() {
				try {
					while (true) {
						updateGame();
						Thread.sleep(50);
					}
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		}.start();
	}

	private void updateGame() {
		if (currentStage != currentStage.nextStage()) {
			currentStage = currentStage.nextStage();
			screen.setStage(currentStage);
			statusBar.setStage(currentStage);
			currentStage.start();
		}

		screen.print();
		currentStage.update();
	}

	public static void main(String[] args) {
		new Game().start();
	}
}