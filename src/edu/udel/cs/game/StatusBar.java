package edu.udel.cs.game;

import edu.udel.cs.entities.Hero;
import edu.udel.cs.stages.IStage;
import edu.udel.cs.utils.IPrintable;
import edu.udel.cs.utils.StringUtils;

public class StatusBar implements IPrintable {
	private Hero hero;
	private IStage stage;
	private String columns;

	public StatusBar(int width, Hero hero, IStage stage) {
		this.hero = hero;
		this.stage = stage;
		this.columns = StringUtils.repeat(" ", (width/2)-5);
	}
	
	public void setStage(IStage stage){
		this.stage = stage;
	}

	@Override
	public String getFrame() {
		return columns + " Stage " + stage.getStageNumber() + 
				"\nHP: " + hero.getHP() + "  MP: " + hero.getMP() + "  Score: " + hero.getScore() +
			    columns + "Time Remaining: " + stage.getTimeRemaining() + 
			    "\n\t j = Move Left  k = Move Right  l = Freeze Objects  q = Quit";
		
	}
}
