package edu.udel.cs.utils;

public interface IPrintable {
	public String getFrame();
}
