package edu.udel.cs.utils;

import java.util.List;

import edu.udel.cs.stages.AbstractStage;

public class GameUtils {
	public void linkStages(List<AbstractStage> stages){
		for (int i = 0; i < stages.size()-1; i++) {
			stages.get(i).setNextLinkedStage(stages.get(i+1));
		}
	}
}
